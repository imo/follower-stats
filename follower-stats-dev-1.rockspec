rockspec_format = "3.0"
package = "follower-stats"
version = "dev-1"
source = {
  url = "git+https://git.kokolor.es/imo/follower-stats.git"
}
description = {
  detailed = "This script returns the domains and what percentage of your followers belong to them.",
  homepage = "https://git.kokolor.es/imo/follower-stats",
  license = "WTFPL",
  maintainer = "Sebastian Huebner <sh@kokolor.es>"
}
dependencies = {
  "lua >= 5.1, < 5.4",
  "lua-cjson == 2.1.0-1",
  "lua-requests"
}
build = {
  type = "none",
  install = {
    bin = { ["follower-stats"] = "follower-stats.lua" }
  }
}
