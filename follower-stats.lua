#!/usr/bin/env lua

local requests = require('requests')

-- parses the given fediverse handle
local function check_handle(handle)
  assert(handle, 'Invalid fediverse handle!')
  local user, domain = handle:match('^(.*)@(.*)$')
  assert(user and domain,
    'Can\'t parse the given fediverse handle. (Example: user@instance.tld)')

  return user, domain
end

-- get the software type - pleroma or mastodon
local function get_software_type(url)
  local resp, err = requests.get(string.format('https://%s/api/v1/instance', url)).json()
  if not resp then
    error('The following error occured: ' .. err)
  end

  if resp.version:find('Pleroma') then
    return 'pleroma'
  else
    return 'mastodon'
  end
end

-- get the user id of the user - need to get the followers and followees
-- TODO: Maybe find a better way to do this
local function get_user_id(url, user, software)
  local urls = {
    pleroma = string.format('https://%s/api/users/show.json?user_id=%s', url, user),
    mastodon = string.format('https://%s/.well-known/webfinger?resource=acct:%s@%s', url, user, url)
  }

  local resp, err = requests.get(urls[software]).json()
  if not resp then
    error('The following error occured: ' .. err)
  end

  if software == 'mastodon' then
    return tonumber(resp.links[3].href:match('^.*/api/salmon/(%d+)$'))
  else
    return tonumber(resp.id)
  end
end

-- calculate percent value for domains
local function percentage(stats)
  local p_stats = {
    followers = {},
    following = {}
  }

  for tname, tbl in pairs(stats) do
    local total = stats[tname].total

    for dom, val in pairs(tbl) do
      if dom ~= 'total' then
        table.insert(p_stats[tname], { dom = dom, perc = (val * 100) / total })
      end
    end
  end

  for tname in pairs(p_stats) do
    table.sort(p_stats[tname], function(a, b)
      return a.perc > b.perc or a.perc == b.perc and a.dom > b.dom
    end)
  end

  return p_stats
end

-- main function
local function main(url, user, access_token)
  local stats = {
    followers = {
      total = 0
    },
    following = {
      total = 0
    }
  }
  local software = get_software_type(url)
  local id = get_user_id(url, user, software)
  local headers = {}

  if software == 'mastodon' then
    headers['Authorization'] = 'Bearer ' .. access_token
    print('Mastodon is currently WIP')
    os.exit()
  end

  -- get list of followers and followees
  for tname in pairs(stats) do
    local resp, err = requests.get({
      string.format('https://%s/api/v1/accounts/%d/%s', url, id, tname ),
      headers = headers
    }).json()
    err = resp.error or err
    if not resp or resp.error then
      error('The following error occured: ' .. err)
    end

    -- iterate over the received list and add or raise the count for the tld's
    for _, val in pairs(resp) do
      local domain = val.acct:match('^.*@(.*)$') or 'local'

      if stats[tname][domain] then
        stats[tname][domain] = stats[tname][domain] + 1
      else
        stats[tname][domain] = 1
      end
      stats[tname].total = stats[tname].total + 1
    end
  end

  return percentage(stats)
end

local user, domain = check_handle(arg[1])
local access_token = arg[2]

local stuff = main(domain, user, access_token)

-- print out the list
for n, t in pairs(stuff) do
  print(string.format('Percentage overview for %s:', n))
  for _, p in ipairs(t) do
    print(string.format('  %s: %.2f%%', p.dom, p.perc))
  end
end