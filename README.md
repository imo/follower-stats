# follower-stats

This script returns the domains and what percentage of your followers belong to them.

## Dependencies

* lua (>=5.1)
* [lua-requests](https://github.com/JakobGreen/lua-requests)

## Install

* [Install luarocks](https://github.com/luarocks/luarocks/wiki/Documentation#Quick_start)
* You need luarocks **>=3.0** to follow the following install instructions

### Method one

* Download and install using the rockspec directly from the repository: `sudo luarocks install https://git.kokolor.es/imo/follower-stats/raw/branch/master/follower-stats-dev-1.rockspec`
* The script should be installed in `/usr/local/bin/`, so you can use it like this: `follower-stats user@domain.tld`
* You can also use the `--local` option, if you don't want to install it global. If you do so, the script is install in `~/.luarocks/` and you have to use the full path to use it

### Method two

* Clone this git
* Change into the cloned folder
* Install dependencies:

```
luarocks init
luarocks init --reset
luarocks build --only-deps
```

**Note:** If the build process of the _xml_ module fails, because it can't find the _libstdc++_ library, you have to specify the path to the library like this:
`luarocks build --only-deps STDCPP_LIBDIR=/usr/lib/gcc/x86_64-linux-gnu/5` (this path works for me at Ubuntu 16.04)
* After the installation is complete you can run it like this: `./lua follower-stats.lua user@domain.tld`

After a few seconds you should get an output similar to this:

```
Percentage overview for followers:
  muh.social: 80.00%
  humu.social: 10.00%
  ...
Percentage overview for following:
  humu.social: 30.00%
  muh.social: 60.00%
  ...
```

## Works with

* Pleroma (if everyone is allowed to see your followers and who you follow)

## TODO

* Authentication to work with Mastodon and Pleroma (to get the stats even if followers and followees are hidden)
* Maybe find a cleaner way to find out the user id
* Clean up
